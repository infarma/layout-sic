package arquivoDePedido

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

type ArquivoDePedido struct {
	Pedido []Pedido `json:"Pedido"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		vector := strings.Split(string(runes), "|")
		pedido := Pedido{}

		pedido.IdCampanha = int32(ConvertStringToInt(vector[0]))
		pedido.IdPedido = int32(ConvertStringToInt(vector[1]))
		pedido.DataHoraPedido = int64(ConvertStringToInt(vector[2]))
		pedido.CnpjCliente = vector[3]
		pedido.CodigoCliente = vector[4]
		pedido.IdentificacaoCampanhaOfertante = vector[5]
		pedido.IdItemPedido = int32(ConvertStringToInt(vector[6]))
		pedido.IdPrazo = int64(ConvertStringToInt(vector[7]))
		pedido.CodigoEan = int64(ConvertStringToInt(vector[8]))
		pedido.QuantidadePedida = int32(ConvertStringToInt(vector[9]))

		if(len(vector) != 10) {
			pedido.CodigoCD = vector[10]
			pedido.CnpjCD = vector[11]
		}

		arquivo.Pedido = append(arquivo.Pedido, pedido)
	}
	return arquivo, err
}

func ConvertStringToInt(valor string) int {
	v, _ := strconv.Atoi(valor)
	return v
}