package arquivoDePedido

type Pedido struct {
	IdCampanha                     int32 `json:"IdCampanha"`
	IdPedido                       int32 `json:"IdPedido"`
	DataHoraPedido                 int64 `json:"DataHoraPedido"`
	CnpjCliente                    string `json:"CnpjCliente"`
	CodigoCliente                  string `json:"IdPedido"`
	IdentificacaoCampanhaOfertante string `json:"DataHoraPedido"`
	IdItemPedido                   int32 `json:"IdItemPedido"`
	IdPrazo                        int64 `json:"IdPrazo"`
	CodigoEan                      int64 `json:"CodigoEan"`
	QuantidadePedida               int32 `json:"QuantidadePedida"`
	CodigoCD                       string `json:"CodigoCD"`
	CnpjCD                         string `json:"CnpjCD"`
}
