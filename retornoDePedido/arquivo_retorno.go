package retornoDePedido

import "fmt"

func RetornoDePedido(idPedido int32, idItemPedido int32, quantidadPedido int32, mensagemNaoAtendimento string) string{
	retornoDePedido := fmt.Sprint(idPedido) + "|"
	retornoDePedido += fmt.Sprint(idItemPedido) + ";" + fmt.Sprint(quantidadPedido) + "|"
	retornoDePedido += mensagemNaoAtendimento
	retornoDePedido += "<Cr><Lf>"

	return retornoDePedido
}
