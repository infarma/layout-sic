package retornoDePedido

type Pedido struct {
	IdPedido                       string `json:"IdPedido"`
	IdItemPedidoQuantidadeAtendida string `json:"IdItemPedidoQuantidadeAtendida"`
	MensagemNaoAtendimento         string `json:"MensagemNaoAtendimento"`
}
