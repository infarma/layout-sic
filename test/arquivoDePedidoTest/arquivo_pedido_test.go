package arquivoDePedidoTest

import (
	"layout-sic/arquivoDePedido"
	"os"
	"testing"
)

func TestGetStruct(t *testing.T) {
	file, err := os.Open("../fileTest/R_20160219150314_10545_10607.TXT")
	if err != nil {
		t.Error("não abriu arquivo", err)
	}

	arquivo, err := arquivoDePedido.GetStruct(file)

	//Arquivo de Recebimento de Pedidos
	var registro arquivoDePedido.Pedido
	registro.IdCampanha = 5749
	registro.IdPedido = 10545
	registro.DataHoraPedido = 111120150802
	registro.CnpjCliente = "08089414000140"
	registro.CodigoCliente = "IDLOJATST"
	registro.IdentificacaoCampanhaOfertante = "85"
	registro.IdItemPedido = 19862
	registro.IdPrazo = 49
	registro.CodigoEan = 7896714223964
	registro.QuantidadePedida = 5

	if registro.IdCampanha != arquivo.Pedido[0].IdCampanha {
		t.Error("ID Campanha não é compativel")
	}
	if registro.IdPedido != arquivo.Pedido[0].IdPedido {
		t.Error("ID Pedido não é compativel")
	}
	if registro.DataHoraPedido != arquivo.Pedido[0].DataHoraPedido {
		t.Error("Data Hora Pedido não é compativel")
	}
	if registro.CnpjCliente != arquivo.Pedido[0].CnpjCliente {
		t.Error("Cnpj Cliente não é compativel")
	}
	if registro.CodigoCliente != arquivo.Pedido[0].CodigoCliente {
		t.Error("Codigo Cliente não é compativel")
	}
	if registro.IdentificacaoCampanhaOfertante != arquivo.Pedido[0].IdentificacaoCampanhaOfertante {
		t.Error("Identificacao Campanha Ofertante não é compativel")
	}
	if registro.IdItemPedido != arquivo.Pedido[0].IdItemPedido {
		t.Error("ID Item Pedido não é compativel")
	}
	if registro.IdPrazo != arquivo.Pedido[0].IdPrazo {
		t.Error("ID Prazo não é compativel")
	}
	if registro.CodigoEan != arquivo.Pedido[0].CodigoEan {
		t.Error("Codigo Ean não é compativel")
	}
	if registro.QuantidadePedida != arquivo.Pedido[0].QuantidadePedida {
		t.Error("Quantida de Pedida não é compativel")
	}
}