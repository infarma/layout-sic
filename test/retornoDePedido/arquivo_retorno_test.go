package retornoDePedido

import (
	"layout-sic/retornoDePedido"
	"testing"
)

func TestRetornoDePedido(t *testing.T){
	idPedido := 19
	idItemPedido := 30
	quantidadPedido := 20
	mensagemNaoAtendimento := ""

	retornoDePedido := retornoDePedido.RetornoDePedido(int32(idPedido), int32(idItemPedido), int32(quantidadPedido), mensagemNaoAtendimento)

	if len(retornoDePedido) != 17 {
		t.Error("Cabecalho não tem o tamanho adeguado")
	}else{
		if retornoDePedido != "19|30;20|<Cr><Lf>" {
			t.Error("Cabecalho não é compativel")
		}
	}
}